/*
Program:        main.cpp
Author:         Seth Tucker
Date:           9/19/2017
Last Modified:  11/2/2017
*/

#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
#include <algorithm>
#include <limits>       // For setting floats and ints to infinity
#include <set>
#include <map>

#include "gnuplot_i.hpp"

using std::cout;
using std::endl;
using std::ifstream;
using std::vector;
using std::string;
using std::pair;
using std::make_pair;
using std::remove;
using std::numeric_limits;
using std::ostream;
using std::ofstream;
using std::set;
using std::map;

/* Setup data structure to represent a point defined by an x and y coordinate.
 */
struct point {
    float x, y;
};

/* Overloaded ouput operator to print points
 */
ostream& operator << (ostream& os, const point p) {
    os << "(" << p.x << ", " << p.y << ")";
    return os;
}

/* overloaded comparison operators to compare points for equality
 */
bool operator == (const point& p1, const point& p2) {
    return(p1.x == p2.x && p1.y == p2.y);
}

bool operator!= (const point& p1, const point& p2) {
    return !(p1 == p2);
}

// Global variables for infinity
float INFINIFLOAT = numeric_limits<float>::infinity();
long INFINILONG = numeric_limits<long>::infinity();


/*************************************/
/***        GNUPlot Functions      ***/
/*************************************/

/* Calls GNUPlot and graphs the boundary points
 */
void plotField(vector<point>& fieldPoints) {
    Gnuplot gp;
    vector<float> x1, y1;
    for (const auto &elem : fieldPoints) {
        x1.push_back(elem.x);
        y1.push_back(elem.y);
    }
    gp.cmd("set size square");
    gp.set_grid().set_xautoscale().set_yautoscale();
    gp.set_style("lines").plot_xy(x1, y1);
    system("pause");

} // End plotField()

/* Calls GNUPlot and graphs the boundary points and bounding box
 */
void plotBox(vector<point>& fieldPoints, vector<point>& box) {
    Gnuplot gp;
    vector<float> x1, y1, x2, y2;
    for (const auto &elem : fieldPoints) {
        x1.push_back(elem.x);
        y1.push_back(elem.y);
    }
    for (const auto &elem : box) {
        x2.push_back(elem.x);
        y2.push_back(elem.y);
    }
    gp.cmd("set size square");
    gp.set_grid().set_xautoscale().set_yautoscale();
    gp.set_style("lines").plot_xy(x1, y1)
      .set_style("lines").plot_xy(x2, y2);
    system("pause");

} // End plotBox()

/* Calls GNUPlot and graphs the boundary points, bounding box, and lattice
 */
void plotLattice(vector<point>& fieldPoints, vector<point>& box, vector<point>& lat) {
    Gnuplot gp;
    vector<float> x1, y1, x2, y2, x3, y3;
    for (const auto &elem : fieldPoints) {
        x1.push_back(elem.x);
        y1.push_back(elem.y);
    }
    for (const auto &elem : box) {
        x2.push_back(elem.x);
        y2.push_back(elem.y);
    }
    for (const auto &elem : lat) {
        x3.push_back(elem.x);
        y3.push_back(elem.y);
    }
    gp.cmd("set size square");
    gp.set_grid().set_xautoscale().set_yautoscale();
    gp.set_style("lines").plot_xy(x1, y1)
      .set_style("lines").plot_xy(x2, y2)
      .set_style("points").plot_xy(x3, y3);
    system("pause");

} // End plotLattice()

/* Calls GNUPlot and graphs the boundary points, boundary box, and path
 */
void plotPath(vector<point>& fieldPoints, vector<point>& box, vector<point>& path) {
    Gnuplot gp;
    vector<float> x1, y1, x2, y2, x3, y3;
    for (const auto &elem : fieldPoints) {
        x1.push_back(elem.x);
        y1.push_back(elem.y);
    }
    for (const auto &elem : box) {
        x2.push_back(elem.x);
        y2.push_back(elem.y);
    }
    for (const auto &elem : path) {
        x3.push_back(elem.x);
        y3.push_back(elem.y);
    }
    gp.cmd("set size square");
    gp.set_grid().set_xautoscale().set_yautoscale();
    gp.set_style("lines").plot_xy(x1, y1)
      .set_style("lines").plot_xy(x2, y2)
      .set_style("lines").plot_xy(x3, y3);
    system("pause");

} // End plotPath()


/****************************************/
/***        Create Bounding Box       ***/
/****************************************/

/* Creates a bounding box around the field that will be used for
 * setting up the integer lattice.
 */
vector<point> createBoundingBox(vector<point>& fieldPoints) {
    // Find the farthest north, south, east, and west points
    vector<point> farthestPoints;
    vector<point> boundingBoxPoints;

    // Northern first
    point northernMost;
    northernMost.x = 0;
    northernMost.y = 0;
    for (const auto &point : fieldPoints) {
        if (point.y > northernMost.y) {
            northernMost.x = point.x;
            northernMost.y = point.y;
        }
    }
    farthestPoints.push_back(northernMost);

    // Southern next
    point southernMost;
    southernMost.x = northernMost.x;
    southernMost.y = northernMost.y;
    for (const auto &point : fieldPoints) {
        if (point.y < southernMost.y) {
            southernMost.x = point.x;
            southernMost.y = point.y;
        }
    }
    farthestPoints.push_back(southernMost);

    // Eastern next
    point easternMost;
    easternMost.x = 0;
    easternMost.y = 0;
    for (const auto &point : fieldPoints) {
        if (point.x > easternMost.x) {
            easternMost.x = point.x;
            easternMost.y = point.y;
        }
    }
    farthestPoints.push_back(easternMost);

    // Western next
    point westernMost;
    westernMost.x = easternMost.x;
    westernMost.y = easternMost.y;
    for (const auto &point : fieldPoints) {
        if (point.x < westernMost.x) {
            westernMost.x = point.x;
            westernMost.y = point.y;
        }
    }
    farthestPoints.push_back(westernMost);

    /* Get x y coordinates for the corners of the bounding box.
     * UL = upper left, BR = bottom right, etc.
     * Upper left x coord is the x coord of the farthest WEST point,
     * Upper left y coord is the y coord of the farthest NORTH point.
     */
    point UL, UR, BL, BR;
    UL.y = northernMost.y;
    UL.x = westernMost.x;
    boundingBoxPoints.push_back(UL);
    UR.y = northernMost.y;
    UR.x = easternMost.x;
    boundingBoxPoints.push_back(UR);
    BR.y = southernMost.y;
    BR.x = easternMost.x;
    boundingBoxPoints.push_back(BR);
    BL.y = southernMost.y;
    BL.x = westernMost.x;
    boundingBoxPoints.push_back(BL);

    // Connect BL point to UL point to close the bounding box
    boundingBoxPoints.push_back(UL);
    return boundingBoxPoints;

} // End createBoundingBox()


/******************************************/
/***        Create Integer lattice      ***/
/******************************************/

/* Creates an intger lattice inside the bounding box to represent the sample
 * locations. The spacing between each sample location will determine the
 * spacing between all points in the lattice.
 */
vector<point> createLattice(vector<point>& boundingBox, int spacing) {
    vector<point> lattice;
    /* Determine the height and width of the bounding box.
     * The corners defining the points of the box are inserted
     * clockwise starting with the upper left corner.
     */
    long xEnd = boundingBox[1].x + 1;
    long yEnd = boundingBox[1].y + 1;

    // Find the starting points for the for loop indices
    long xIndex = boundingBox[3].x + spacing;
    long yIndex = boundingBox[3].y + spacing;

    point temp;
    while (xIndex < xEnd) {
        while (yIndex < yEnd) {
            temp.x = xIndex;
            temp.y = yIndex;
            lattice.push_back(temp);
            yIndex += spacing;
        }
        xIndex += spacing;
        yIndex = boundingBox[3].y + spacing;
        if ((xEnd - xIndex) < spacing) {
            break;
        }
    }
    return lattice;

} // End createLattice()


/*******************************************/
/***       Cleanup Sample Locations      ***/
/*******************************************/

/* A very elegant solution to detect for line segment intersections
 * Source: www.bryceboe.com/2006/10/23/line-segment-intersection-algorithm/
 */

/* Helper function for finding Ray line Segment Intercepts. Calculates if
 * the line segment formed by points A, B, and C is clockwise or counter-clockwise.
 */
bool calcClockWise(point A, point B, point C) {
    return (C.y - A.y) * (B.x - A.x) > (B.y - A.y) * (C.x - A.x);

} // End ccw()

/* Helper function for finding Ray Line Segment Intercepts. Groups the four
 * points into 4 groups of 3 and passes them to the clockwise function
 */
bool intersect(point A, point B, point C, point D) {
    return (calcClockWise(A, C, D) != calcClockWise(B, C, D)) &&
           (calcClockWise(A, B, C) != calcClockWise(A, B, D));

} // End intersect()

/* Takes every point in the integer lattice, creates a new point directly
 * north of that point, then checks if the line crated by these two points
 * crosses any field boundaries and counts the number of intersections.
 * If there are an odd number of intersections, then the selected point is
 * inside the field and is kept. If the point has an even number of intersections
 * it is outside the field and discarded.
 */
vector<point> rayLineSegmentIntercept(vector<point>& lattice, vector<point>& fieldPoints, vector<point>& bBox) {
    vector<point> sampleLocations;
    point A, B, C, D;
    for (auto i = 0u; i < lattice.size(); i++) {
        int numIntersects = 0;
        A.x = lattice[i].x; A.y = lattice[i].y;
        B.x = lattice[i].x; B.y = bBox[0].y + 1;
        for (auto j = 0u; j < fieldPoints.size() - 2; j++) {
            C.x = fieldPoints[j].x; C.y = fieldPoints[j].y;
            D.x = fieldPoints[j + 1].x; D.y = fieldPoints[j + 1].y;
            bool doesItIntersect = intersect(A, B, C, D);
            if (doesItIntersect == true) { numIntersects += 1; }
        }
        if (numIntersects % 2 != 0) {
            sampleLocations.push_back(lattice[i]);
        }
    }
    return sampleLocations;

} // End rayLineSegmentIntercept()


/***************************************/
/***        Adjacency Matrix        ****/
/***************************************/

/* Helper function for createAdjacencyMatrix() that finds the distance between two points.
 */
float findDistance(point start, point end) {
    float distance = sqrt((end.x - start.x)*(end.x - start.x) +
                          (end.y - start.y)*(end.y - start.y));
    return distance;

} // End of findDistance()

/* Takes the sample locations inside the field and returns an adjacency matrix of the distances from
 * each point to every other point. Also checks if the line between two points crosses a field boundary,
 * if it does, the distance between those two points is set to infinity.
 */
vector<vector<float>> createAdjacencyMatrix(vector<point>& sampleLocations, vector<point>& fieldPoints) {
    // Number of rows and columns is the number of sample locations. The matrix is square
    int rows = sampleLocations.size();
    int columns = sampleLocations.size();
    point A, B, C, D;

    // Initialize adjacency matrix with all values 0;
    vector<vector<float>> adjMatrix(rows, vector<float>(columns, 0));

    for (auto i = 0; i < rows; i++) {
        // Start index j at index i because we don't need to recheck points
        for (auto j = i; j < columns; j++) {
            // The distance from one point to itself is alwasy 0
            if (i == j) {
                adjMatrix[i][j] = 0;
            } else {
                // Get the distance between two current points
                float distance = findDistance(sampleLocations[i], sampleLocations[j]);
                // Check to make sure path between current points doesn't go outside the field
                A.x = sampleLocations[i].x; A.y = sampleLocations[i].y;
                B.x = sampleLocations[j].x; B.y = sampleLocations[j].y;
                for (auto k = 0u; k < fieldPoints.size() - 2; k++) {
                    C.x = fieldPoints[k].x; C.y = fieldPoints[k].y;
                    D.x = fieldPoints[k + 1].x; D.y = fieldPoints[k + 1].y;
                    // If they intersect set the value of the adjacency Matrix at [i][j] and [j][i]
                    // to infinity
                    bool doesItIntersect = intersect(A, B, C, D);
                    if (doesItIntersect) {
                        adjMatrix[i][j] = INFINIFLOAT;
                        adjMatrix[j][i] = INFINIFLOAT;
                        break;
                    } else { // If they don't intersect add the distance to the matrix
                        adjMatrix[i][j] = distance;
                        adjMatrix[j][i] = distance;
                    }
                }
            }
        }
    }

    return adjMatrix;

} // End createAdjacencyMatrix()


/******************************/
/***        Plot path       ***/
/******************************/

/* Helper function for path finding that finds the starting point
 */
point findStartPoint(vector<point>& nodes, point fieldStart) {
    float diffX, diffY, newDistance;
    float oldDistance = 100000;
    point startPoint;
    for (auto i = 0u; i < nodes.size(); i++) {
        diffX = nodes[i].x - fieldStart.x;
        diffY = nodes[i].y - fieldStart.y;
        newDistance = sqrt((diffY * diffY) + (diffX * diffX));
        if (newDistance < oldDistance) {
            oldDistance = newDistance;
            startPoint = nodes[i];
        }
    }
    return startPoint;

} // End findStartPoint()

/* Helper function for path finding. Takes the current point and finds its
 * immediate north, south, east, and west neighbors if they exist. A neighboring
 * point is eligible if it is within the distance defined by 'spacing' which represents
 * the distance between sample locations. A north neighbor has the same x-coordinate
 * and a y-coordinate that is 'spacing' distance above the original point.
 * Ex: originalPoint = (15, 15)
       northNeighbor = (15, 15 + spacing)
 */
vector<point> findNeighbors(point& current, vector<point>& unvisitedPoints, int& spacing) {
    vector<point> neighbors;
    // Initialize neighbor points, if they don't exist ther coords will remain (0, 0)
    point North, South, East, West;
    North.x = 0; North.y = 0;
    South.x = 0; South.y = 0;
    East.x = 0; East.y = 0;
    West.x = 0; West.y = 0;
    for (auto i = 0u; i < unvisitedPoints.size(); i++) {
        // Nearest NORTH neighbor
        if (unvisitedPoints[i].x == current.x && unvisitedPoints[i].y == current.y + spacing) {
            North = unvisitedPoints[i];
        }
        // Nearest SOUTH neighbor
        else if (unvisitedPoints[i].x == current.x && unvisitedPoints[i].y == current.y - spacing) {
            South = unvisitedPoints[i];
        }
        // Nearest EAST neighbor
        else if (unvisitedPoints[i].x == current.x + spacing && unvisitedPoints[i].y == current.y) {
            East = unvisitedPoints[i];
        }
        // Nearest WEST neighbor
        else if (unvisitedPoints[i].x == current.x - spacing && unvisitedPoints[i].y == current.y) {
            West = unvisitedPoints[i];
        }
    }

    //.push_back(North);
    //neighbors.push_back(East);
    neighbors.push_back(North);
    neighbors.push_back(East);
    neighbors.push_back(South);
    neighbors.push_back(West);
    return neighbors;

} // End findNeighbors()

/* Helper function for path finding. Removes the current point from the list
 * of unvisited points and returns the new list. Uses a lambda function as
 * there is no easy way to remove a user defined data structure from a
 * vector of those same structures. Its painful to look at but it does work.
 */
vector<point> removePoint(point& current, vector<point>& unvisitedPoints) {
    unvisitedPoints.erase(remove_if(unvisitedPoints.begin(), unvisitedPoints.end(),
                         [&current](point const &p) { // Magic...
                             return p.x == current.x && p.y == current.y;
                         }), unvisitedPoints.end());
    return unvisitedPoints;

} // End removePoint()

/* Takes all sample locations inside the field and creates a path that will
 * visit all of them. Uses brute force to find the path.
 */
vector<point> findPath(vector<point>& sampleLocations, int& spacing, point& fieldStart){
    vector<point> path, currentNeighbors;
    vector<point> unvisitedPoints = sampleLocations;
    point current;

    // Find closest sample location to field start point
    point startPoint = findStartPoint(unvisitedPoints, fieldStart);

    // Remove the start/end point from the vector of unvisited nodes.
    unvisitedPoints = removePoint(startPoint, unvisitedPoints);

    current = startPoint;
    path.push_back(current);

    // This is used when neighbors don't exist and backtracking is necessary
    int i = 2;

    while (unvisitedPoints.size() > 0) {
        // Used to calculate when backtracking is necessary
        int nullNeighbors = 1;

        // Get all immediate neighbors of the current point
        currentNeighbors = findNeighbors(current, unvisitedPoints, spacing);

        for (const auto &neighbor : currentNeighbors) {
            if (neighbor.x != 0 && neighbor.y != 0) {
                // Reset backtracking variable
                i = 2;
                // North
                if (neighbor.x == current.x && neighbor.y == current.y + spacing) {
                    current = neighbor;
                    unvisitedPoints = removePoint(current, unvisitedPoints);
                    path.push_back(current);
                    break;
                }
                // East
                else if (neighbor.x == current.x + spacing && neighbor.y == current.y) {
                    current = neighbor;
                    unvisitedPoints = removePoint(current, unvisitedPoints);
                    path.push_back(current);
                    break;
                }
                //South
                else if (neighbor.x == current.x && neighbor.y == current.y - spacing) {
                    current = neighbor;
                    unvisitedPoints = removePoint(current, unvisitedPoints);
                    path.push_back(current);
                    break;
                }
                // West
                else if (neighbor.x == current.x - spacing && neighbor.y == current.y) {
                    current = neighbor;
                    unvisitedPoints = removePoint(current, unvisitedPoints);
                    path.push_back(current);
                    break;
                }
            }
            /* Backtrack if necessary. Increments the backtracking variable each
             * time for when more than one step backward is needed.
             */
            else if (nullNeighbors == 4 && unvisitedPoints.size() > 0) {
                current = path[path.size() - i];
                i += 1;
                break;
            }
            /* Each time a non-existing neighbor is found, a.k.a. a neighbor whose coords
             * are (0, 0), increment this variable once. Used for when no neighbor exists
             * and backtracking is necessary.
             */
            else { nullNeighbors++; }
        }
    }
    return path;

} // End findPath()

/*
vector<point> findBetterPath(vector<vector<float>>& adjMatrix, vector<point>& sampleLocations) {

}
*/

/**************************************/
/***        Return to Start         ***/
/**************************************/

/* Uses Dijkstra's Algorithm to find the shortest path from the last sample location back to start
 */
vector<point> returnToStart(vector<point>& sampleLocations, vector<point>& path, vector<vector<float>>& adjMatrix) {

    vector<point> pathToStart;
    long matrixSize = adjMatrix.size();

    // Get the last sample location visited which is the start point for Dijkstra's and the first
    // sample location which is the end point
    point sourcePoint, targetPoint;
    long source, target;
    for (auto i = 0u; i < sampleLocations.size(); i++) {
        // The last point in the path is NOT always the last sample location
        if (path[path.size() - 1].x == sampleLocations[i].x &&
            path[path.size() - 1].y == sampleLocations[i].y) {
            sourcePoint = sampleLocations[i];
            source = i;
            cout << "Source: " << source << " " << sourcePoint << endl;
        }
        // The first point in the path is NOT always the first sample location
        if (path[0].x == sampleLocations[i].x && path[0].y == sampleLocations[i].y) {
            targetPoint = sampleLocations[i];
            target = i;
            cout << "Target: " << target << " " << targetPoint << endl;
        }
    }

    /***    Here There Be Dijkstra's Algorithm and Dragons   ***/

    /* Psuedocode pulled from Wikipedia, https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm */

    // Create dist[] and prev[]
    vector<float> dist(matrixSize);
    vector<long> prev(matrixSize);

    // create vertex set Q
    vector<long> Q;

    // for each vertex v in graph              // Initialization
    for (auto v = 0; v < matrixSize; v++) {
        // dist[v] <- INFINITY                 // Unkown distance from source to v
        dist[v] = INFINIFLOAT;
        // prev[v] <- UNDEFINED                // Previous node in optimal path from source
        prev[v] = INFINILONG;
        // add v to Q
        Q.push_back(v);
    }
    // dist[source] = 0                        // Distance from source to source
    dist[source] = 0;

    // while Q is not empty:
    while (Q.size() > 0) {
        float tempDist = INFINIFLOAT;
        // u <- vertex in Q with min dist[u]   // Node with least dist will be selected first
        long u;
        for (const auto& elem : Q) {
            if (dist[elem] < tempDist) {
                tempDist = dist[elem];
                u = elem;
            }
        }
        if (u == target) { break; }
        // remove u from Q
        Q.erase(remove(Q.begin(), Q.end(), u), Q.end());
        // for each neighbor v of u:           // Where v is still in Q
        for (auto i = 0u; i < Q.size(); i++) {
            long v = Q[i];
            // alt <- dist[u] + lenght(u, v)
            float alt = dist[u] + adjMatrix[u][v];
            // if alt < dist[v]:               // A shorter path to v has been found
            if (alt < dist[v]) {
                // dist[v] <- alt
                dist[v] = alt;
                // prev[v] <- u
                prev[v] = u;
            }
        }
    }

    // S ← empty sequence
    // Here S is already defined and called pathToStart
    // u ← target
    long u = target;
    // while prev[u] is defined:                  // Construct the shortest path with a stack S
    while (true) {
        // insert u at the beginning of S         // Push the vertex onto the stack
        pathToStart.insert(pathToStart.begin(), sampleLocations[u]);
        // u ← prev[u]                            // Traverse from target to source
        if (prev[u] == INFINILONG) { break; }
        u = prev[u];
    }

    /***                                ***/

    return pathToStart;

} // End returnToStart()


/**************************/
/***        Main        ***/
/**************************/

int main(int argc, char* argv[]) {
    // Find and open text file containing points
    string inputFile(argv[1]);
    int spacing;
    ifstream source;
    source.open(inputFile);
    vector<point> fieldPoints, boundingBox, lattice, sampleLocations, path, pathToStart;
    vector<vector<float>> adjMatrix;

    // Get sample spacing from text file
    source >> spacing;

    // Get points from text file
    if (source) {
        point input;
        char comma;
        while (source >> input.x >> comma >> input.y) {
            fieldPoints.push_back(input);
        }
    }
    // Last point must connect back to first point
    fieldPoints.push_back(fieldPoints[0]);
    plotField(fieldPoints);

    // Create bounding box around field
    boundingBox = createBoundingBox(fieldPoints);
    plotBox(fieldPoints, boundingBox);

    // Populate field with integer lattice
    lattice = createLattice(boundingBox, spacing);
    // Plot data without cleaning up sample locations
    plotLattice(fieldPoints, boundingBox, lattice);

    // Determine which sample points are "inside" the field
    sampleLocations = rayLineSegmentIntercept(lattice, fieldPoints, boundingBox);
    // Plot data with "cleaned up" list of sample locations
    plotLattice(fieldPoints, boundingBox, sampleLocations);

    // Create an adjacency matrix of the sample locations
    adjMatrix = createAdjacencyMatrix(sampleLocations, fieldPoints);

    // Determine the path to each sample locations
    path = findPath(sampleLocations, spacing, fieldPoints[0]);
    // Plot data with path to sample locations
    plotPath(fieldPoints, boundingBox, path);

    // Determine the path back to the starting locations
    pathToStart = returnToStart(sampleLocations, path, adjMatrix);
    // Plot data with return path to starting
    plotPath(fieldPoints, boundingBox, pathToStart);

    source.close();
    cout << endl;
    return 0;

} // End main()
