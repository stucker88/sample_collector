'''
Program:		pointGen.py
Author:			Seth Tucker
Date:			9/28/2017
Last Modified:	9/28/2017

Generates at least 4 points to use as test input
'''

import random
import math

def main():
    # Need at least 4 points and for now no more than 100
    numPoints = random.randint(4, 8)

    # Random starting point
    x = random.randint(0, 100)
    y = random.randint(0, 100)
    originX = x
    originY = y

    # Random angle between origin point, new point, and x axis
    angleMin = random.randint(75, 90)
    angleMax = random.randint(90, 105)
    angle = random.randint(angleMin, angleMax)

    # Random length between origin point and new point
    length = random.randint(50, 200)

    pointList = []
    outFile = open("points.txt", "w")

    # Random spacing between sample locations
    spacing = random.randint(10, 50)

    outFile.write(str(spacing) + "\n")
    outFile.write(str(x) + ", " + str(y) + "\n")

    # Create randomly located points
    for i in range(numPoints):
      endx = round(x + (length * math.cos(math.radians(angle))))
      endy = round(y + (length * math.sin(math.radians(angle))))
      pointList.append((endx, endy))
      point = str(endx) + ", " + str(endy)
      outFile.write(str(endx) + ", " + str(endy) + '\n')
      x = endx
      y = endy
      angleMax -= 45
      angleMin -= 45
      angle = random.randint(angleMin, angleMax)

    # Last point must return to origin point
    pointList.append((originX, originY))
    print(pointList)

    # Finish writing to file and close it
    outFile.write(str(originX) + ", " + str(originY) + '\n')
    outFile.close()

main()
